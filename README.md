This repository contains packaged versions of the Epita ACDC toolkit. It removes the burden of installing, updating and removing the package manually or through a non-standard utility.
Sources can be found at https://github.com/MarcVillain/acdc-toolkit.

To use this repository with pacman, add this to your `/etc/pacman.conf`:

```ini
[epita-acdc]
SigLevel = Optional TrustAll
Server = https://gitlab.com/Fiksd/epita-acdc-pkgs/raw/master/
```

To install the toolkit, simply run:
```bash
sudo pacman -Syu acdc-toolkit
```
