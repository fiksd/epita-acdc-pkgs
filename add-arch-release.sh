#!/bin/sh -e

REPO_NAME=epita-acdc
AR_SUFFIX=.tar.xz
REPO_DIR="$(pwd)"
BUILD_DIR="$(mktemp -d)"
PACKAGE="$1"

trap "{ rm -r '$BUILD_DIR'; }" EXIT


if [ -e "$REPO_DIR/$REPO_NAME.db" ]
then
	cp "$REPO_DIR/$REPO_NAME.db" "$BUILD_DIR/$REPO_NAME.db$AR_SUFFIX"
	cp "$REPO_DIR/$REPO_NAME.files" "$BUILD_DIR/$REPO_NAME.files$AR_SUFFIX"
fi

repo-add -R "$BUILD_DIR/$REPO_NAME.db$AR_SUFFIX" "$PACKAGE"

cp "$PACKAGE" "$REPO_DIR"
mv "$BUILD_DIR/$REPO_NAME.db$AR_SUFFIX" "$REPO_DIR/$REPO_NAME.db"
mv "$BUILD_DIR/$REPO_NAME.files$AR_SUFFIX" "$REPO_DIR/$REPO_NAME.files"
